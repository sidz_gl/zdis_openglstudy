//#include<iostream>
//#include "GLFW/glfw3.h"
//int main()
//{
//	std::cout <<"Hello World!" ;
//	return 0;
//}


#include<glew.h>
#include <GLFW/glfw3.h>
#include<iostream>
#include "Application.h"


//DEfinitiuon
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void ProcessWindowInput(GLFWwindow* window);

bool SetupShader(const char* VS, const char* FS, unsigned int& ShaderProgram);
void DrawTraingle();

/////Drawing TRinagle
#define Drawing
#ifdef Drawing
unsigned int VAO, VBO;
float vertexData[] =
{ -0.5f,-0.5f,0.0f,
	 0.5f,-0.5f,0.0f,
	 0.0f,0.5f,0.0f,
};

const char* VertexShaderContent = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"out vec4 oCol;\n"
"void main()\n"
"{\n"
"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"   oCol = vec4(0.3, 0.0, 0.0, 1.0);\n"
"}\0";
const char* FragmentShaderContent = "#version 330 core\n"
"out vec4 FragColor;\n"
"in vec4 oCol;\n"
"uniform vec4 uCol;\n"
"void main()\n"
"{\n"
"   FragColor =uCol;\n"
"}\n\0";



void DrawTraingle()
{
	//Create Vertex Array Object
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	//SetupShader(VertexShaderContent, FragmentShaderContent);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
}

bool SetupShader(const char* VS, const char* FS, unsigned int& ShaderProgram)
{
	int sucess;
	char infolog[512]{ "None" };
	unsigned int vertexShaderObj;
	unsigned int FragmentShaderObj;
	//unsigned int ShaderProgram;

	vertexShaderObj = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderObj, 1, &VertexShaderContent, NULL);
	glCompileShader(vertexShaderObj);
	glGetShaderiv(vertexShaderObj, GL_COMPILE_STATUS, &sucess);
	if (!sucess)
	{
		glGetShaderInfoLog(vertexShaderObj, 512, NULL, infolog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infolog << std::endl;
		return false;
	}
	FragmentShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(FragmentShaderObj, 1, &FragmentShaderContent, NULL);
	glCompileShader(FragmentShaderObj);
	glGetShaderiv(FragmentShaderObj, GL_COMPILE_STATUS, &sucess);

	if (!sucess)
	{
		glGetShaderInfoLog(FragmentShaderObj, 512, NULL, infolog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infolog << std::endl;
		return false;
	}
	ShaderProgram = glCreateProgram();
	glAttachShader(ShaderProgram, vertexShaderObj);
	glAttachShader(ShaderProgram, FragmentShaderObj);
	glLinkProgram(ShaderProgram);

	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &sucess);
	if (!sucess)
	{
		glGetProgramInfoLog(ShaderProgram, 512, NULL, infolog);
		std::cout << "ERROR::SHADER::ShaderPRogra::LINKING_FAILED\n" << infolog << std::endl;
		return false;
	}

	//Setup the layout also... and attrib

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), (void*)0);
	glEnableVertexAttribArray(0);

	return true;
}


#endif // Drawing


int main(void)
{
	std::cout << "Initializing SIDZ Engine...." << std::endl;
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
	{

		std::cout << "[Error]:" << "Failed to glfwInit()!";
		return -1;
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(800, 600, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		std::cout << "[Error]:" << "Window creation Failed!";
		return -1;
	}
	glfwMakeContextCurrent(window);
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Error in glewInit" << std::endl;
	}
	std::cout << "OpengL Version" << glGetString(GL_VERSION) << std::endl;

	//Settings up callbacks to Windows
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	unsigned int shaderProgram;
	DrawTraingle();//now a VAO is active!
	if (SetupShader(VertexShaderContent, FragmentShaderContent, shaderProgram) == false)
	{
		std::cout << "Unable to setup the Shader!";
		return -1;
	}
	//Getting Unofrom color attribute...
	int uniformLocation = glGetUniformLocation(shaderProgram, "uCol");


	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{

		ProcessWindowInput(window);
		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(shaderProgram);
		/*Uniform Location Testing*/
		double temp_dTime = glfwGetTime();
		float temp_fGreen = (sin(temp_dTime*20.0f) / 2.0f) + 0.5f;

		glUniform4f(uniformLocation, 0.0f, temp_fGreen, 0.0f, 1.0f);

		/*Uniform Location Testing*/


		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

void ProcessWindowInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}



