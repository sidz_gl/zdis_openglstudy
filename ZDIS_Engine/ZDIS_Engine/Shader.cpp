#include "Shader.h"



Shader::Shader(const char * a_VertexPath, const char * a_FragmentPath)
{

	//Read from the File
	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;

	//Letting shader file to throw expections...
	vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		//Open file!
		vShaderFile.open(a_VertexPath);
		fShaderFile.open(a_FragmentPath);
		std::stringstream vShaderStream, fShaderStream;
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();
		vShaderFile.close();
		fShaderFile.close();

		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();



	}
	catch (std::ifstream::failure e)
	{
		std::cout << "[Shader]Error:Coudn't Read the files" << std::endl;
	}

	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();
	//Compiling shader

	//Vertex Shader
	int sucess;
	char infoLog_Vertex[512];

	unsigned int vertexShaderID, fragmentShaderID;
	vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, &vShaderCode, NULL);
	glCompileShader(vertexShaderID);

	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &sucess);
	if (!sucess)
	{
		glGetShaderInfoLog(vertexShaderID, 512, NULL, infoLog_Vertex);
		std::cout << "[Shader]ERROR:vertexShaderID:Compilation Failed\n" << infoLog_Vertex << std::endl;

	}

	//Fragment Shader

	char infoLog_Fragment[512];

	fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &fShaderCode, NULL);
	glCompileShader(fragmentShaderID);


	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &sucess);
	if (!sucess)
	{
		glGetShaderInfoLog(fragmentShaderID, 512, NULL, infoLog_Fragment);
		std::cout << "[Shader]ERROR:fragmentShaderID:Compilation Failed\n" << infoLog_Fragment << std::endl;
	}

	m_iProgramID = glCreateProgram();
	char infoLog_ShaderProgram[512];
	glAttachShader(m_iProgramID, vertexShaderID);
	glAttachShader(m_iProgramID, fragmentShaderID);
	glLinkProgram(m_iProgramID);
	glGetProgramiv(m_iProgramID, GL_LINK_STATUS, &sucess);
	if (!sucess)
	{
		glGetProgramInfoLog(m_iProgramID, 512, NULL, infoLog_ShaderProgram);
		std::cout << "[Shader]ERROR:m_iProgramID:Link Failed\n" << infoLog_ShaderProgram << std::endl;
	}
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);



}

Shader::Shader(const char* a_vShaderCode, const char* f_vShaderCode,int a)
{
	const char* vShaderCode = a_vShaderCode;
	const char* fShaderCode = f_vShaderCode;
	//Compiling shader

	//Vertex Shader
	int sucess;
	char infoLog_Vertex[512];

	unsigned int vertexShaderID, fragmentShaderID;
	vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, &vShaderCode, NULL);
	glCompileShader(vertexShaderID);

	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &sucess);
	if (!sucess)
	{
		glGetShaderInfoLog(vertexShaderID, 512, NULL, infoLog_Vertex);
		std::cout << "[Shader]ERROR:vertexShaderID:Compilation Failed\n" << infoLog_Vertex << std::endl;

	}

	//Fragment Shader

	char infoLog_Fragment[512];

	fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &fShaderCode, NULL);
	glCompileShader(fragmentShaderID);


	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &sucess);
	if (!sucess)
	{
		glGetShaderInfoLog(fragmentShaderID, 512, NULL, infoLog_Fragment);
		std::cout << "[Shader]ERROR:fragmentShaderID:Compilation Failed\n" << infoLog_Fragment << std::endl;
	}

	m_iProgramID = glCreateProgram();
	char infoLog_ShaderProgram[512];
	glAttachShader(m_iProgramID, vertexShaderID);
	glAttachShader(m_iProgramID, fragmentShaderID);
	glLinkProgram(m_iProgramID);
	glGetProgramiv(m_iProgramID, GL_LINK_STATUS, &sucess);
	if (!sucess)
	{
		glGetProgramInfoLog(m_iProgramID, 512, NULL, infoLog_ShaderProgram);
		std::cout << "[Shader]ERROR:m_iProgramID:Link Failed\n" << infoLog_ShaderProgram << std::endl;
	}
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

}

Shader::~Shader()
{
}

void Shader::Use()
{

	glUseProgram(m_iProgramID);
}

void Shader::SetBool(const std::string &a_UniformName, bool a_bValue)
{
	glUniform1i(glGetUniformLocation(m_iProgramID, a_UniformName.c_str()), (int)a_bValue);

}

void Shader::SetFloat(const std::string &a_UniformName, float a_bValue)
{
	glUniform1f(glGetUniformLocation(m_iProgramID, a_UniformName.c_str()), a_bValue);
}

void Shader::SetInt(const std::string &a_UniformName, int a_bValue)
{
	glUniform1i(glGetUniformLocation(m_iProgramID, a_UniformName.c_str()), a_bValue);
}
