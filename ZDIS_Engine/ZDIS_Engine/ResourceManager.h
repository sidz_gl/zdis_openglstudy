
//#ifndef Image_DEC
//
//#endif

#pragma once
#include "Shader.h"
#include "Texture.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

//#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


class ResourceManager
{
public:
	static Shader LoadShader(const char * a_VertexPath, const char * a_FragmentPath);
	static Texture LoaderTexture(const char *a_TexturePath);
	

	~ResourceManager();
private:

	static Texture LoadTextureFromFile(const char* a_TexturePath);
	ResourceManager();
};

