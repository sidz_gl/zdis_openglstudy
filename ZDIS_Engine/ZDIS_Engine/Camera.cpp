#include "Camera.h"





Camera::Camera()
{

}
Camera::Camera(glm::vec3 a_Position, glm::vec3 a_Up, float a_fPitch , float a_fYaw ) :m_Front(glm::vec3(0, 0, -1.0f)), m_fSensitivity(c_fSensitivity), m_fMouseSpeed(c_fSpeed), m_fZoom(c_fZoom)
{

	m_Position = a_Position;
	m_Up = a_Up;
	m_fPitch = a_fPitch;
	m_fYaw = a_fYaw;

	UpdateCameraValues();

}



Camera::~Camera()
{
}

glm::mat4 Camera::GetViewMatrix()
{
	return glm::lookAt(m_Position, m_Position + m_Front, m_Up);

}

void Camera::ProcessKeyboardInput(eCameraMovement a_eMovementType, float a_fdeltaTime)
{
	switch (a_eMovementType)
	{
	case FORWARD:
		m_Position = m_Position + m_Front * m_fMouseSpeed*a_fdeltaTime;
		break;
	case BACKWARD:
		m_Position = m_Position - m_Front * m_fMouseSpeed*a_fdeltaTime;
		break;
	case LEFT:
		m_Position = m_Position - glm::normalize(glm::cross(m_Up, m_Front)) * m_fMouseSpeed*a_fdeltaTime;
		break;
	case RIGHT:
		m_Position = m_Position + glm::normalize(glm::cross(m_Up, m_Front)) * m_fMouseSpeed*a_fdeltaTime;
		break;
	}
}

void Camera::ProcessMouseInput(float a_fXpos, float a_fYpos)
{

	if (m_bFirstMouse) // this bool variable is initially set to true
	{
		m_fPreviousX = a_fXpos;
		m_fPreviousY = a_fYpos;
		m_bFirstMouse = false;
	}

	m_fDeltaX = a_fXpos - m_fPreviousX;
	m_fDeltaY = m_fPreviousY - a_fYpos;

	m_fPreviousX = a_fXpos;
	m_fPreviousY = a_fYpos;

	m_fDeltaX *= m_fSensitivity;
	m_fDeltaY *= m_fSensitivity;


	m_fPitch += m_fDeltaY;
	m_fYaw += m_fDeltaX;

	if (m_fPitch>=89.0f)
	{
		m_fPitch = 89.0f;
	}
	if (m_fPitch <= -89.0f)
	{
		m_fPitch = -89.0f;
	}

	UpdateCameraValues();
}

void Camera::ProcessScrollInput(float a_fYPos)
{
	if (m_fZoom >= 1.0f && m_fZoom <= 60.0f)
	{
		m_fZoom += a_fYPos ;
	}

	if (m_fZoom < 1.0f)
	{
		m_fZoom = 1.0f;
	}

	if (m_fZoom > 60.0f)
	{
		m_fZoom = 60.0f;
	}
}

void Camera::UpdateCameraValues()
{
	//Caculate euler angles
	m_Front.x = cos(glm::radians(m_fPitch))* cos(glm::radians(m_fYaw));
	m_Front.y = sin(glm::radians(m_fPitch));
	m_Front.z = cos(glm::radians(m_fPitch))* sin(glm::radians(m_fYaw));






}
