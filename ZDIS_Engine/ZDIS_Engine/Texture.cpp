#include "Texture.h"

Texture::Texture() :m_iImageHeight(0), m_iImageWidth(0), m_iImgChannel(0), m_iTextureID(1)
{
}

void Texture::GenerateTexture(GLuint a_iWidth, GLuint a_iHeight, unsigned char* a_ImageDAta)
{
	glGenTextures(1, &m_iTextureID);
	glBindTexture(GL_TEXTURE_2D, m_iTextureID);

	//TODO:Expose this later!
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_iImageWidth, m_iImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, a_ImageDAta);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::Bind() const
{
	glBindTexture(GL_TEXTURE_2D, m_iTextureID);
}
