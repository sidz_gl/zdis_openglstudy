#pragma once
#include <glad/glad.h> // include glad to get all the required OpenGL headers

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>



class Texture
{
public:
	Texture();
	void GenerateTexture(GLuint a_iWidth, GLuint a_iHeight,unsigned char *a_ImageDAta);
	void Bind() const;

	int m_iImageHeight, m_iImageWidth, m_iImgChannel;

	unsigned int m_iTextureID;
	
};

