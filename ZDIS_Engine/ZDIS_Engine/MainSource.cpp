

//TODO:Why at top!
#define STB_IMAGE_IMPLEMENTATION


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h> 
#include <iostream>>
#include "Shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ResourceManager.h"
#include "Camera.h"
//#ifndef Image_DEC
//#define STB_IMAGE_IMPLEMENTATION
//#include "stb_image.h"
//#endif

int main();

//FORWARD DECLARATION
void InitGLFW();
GLFWwindow* CreateGLFWWindow();
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void RenderLoop(GLFWwindow* window);
void ProcessInput(GLFWwindow *a_window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;


float VeticesForTexture[] =
{
	//VErtices           //Color         //UV Textures
	 0.5f,0.5f,0.0f,    1.0f,0.0f,0.0f,   1.0f,1.0f,
	 0.5f,-0.5f,0.0f,   0.0f,1.0f,0.0f,   1.0f,0.0f,
	-0.5f,-0.5f,0.0f,   0.0f,0.0f,1.0f,   0.0f,0.0f ,
	-0.5f,0.5f,0.0f,    1.0f,1.0f,0.0f,   0.0f,1.0f
};

float VeticesForSize2Texture[] =
{
	//VErtices           //Color         //UV Textures
	 0.5f,0.5f,0.0f,    1.0f,0.0f,0.0f,   2.0f,2.0f,
	 0.5f,-0.5f,0.0f,   0.0f,1.0f,0.0f,   2.0f,0.0f,
	-0.5f,-0.5f,0.0f,   0.0f,0.0f,1.0f,   0.0f,0.0f ,
	-0.5f,0.5f,0.0f,    1.0f,1.0f,0.0f,   0.0f,2.0f
};
float VerticesForCube[] = {
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};


void CreateCube(unsigned int &a_VAO);

void DrawCube(Shader a_ShaderToUse, unsigned int &a_VAO, glm::mat4 model, unsigned int ModelUniformLocation,
	glm::mat4 view, unsigned int ViewUniformLocation,
	glm::mat4 projection, unsigned int ProjectionUniformLocation,
	unsigned int m_iWoodTextureID,
	unsigned int m_iFaceTextureID);
/**/

unsigned int indices[] = {  // note that we start from 0!
	0, 1, 3,   // first triangle
	1, 2, 3    // second triangle
};




float deltaTime = 0;
float currentTime = 0;
float previousTime = 0;

float m_fMixValue = 0.1f;


glm::vec3 CameraPosition, CameraDirection, CameraUp;

float CameraMoveSpeed = 3.5f;

void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
//Camea declaration
float prevXPos = 0, prevYPos = 0;
float deltaX = 0;
float deltaY = 0;
float pitch = 0;
float yaw = 0;
glm::vec3 cameraLookDirection;
float camFOV = 45.0f;


Camera m_SceneCamera;

int main()
{
	std::cout << "Main Program Started" << std::endl;
	glm::vec4 vec(1.0f, 0.0f, 0.0f, 1.0f);
	glm::mat4 trans = glm::mat4(1.0f);
	trans = glm::translate(trans, glm::vec3(1.0f, 1.0f, 0.0f));
	vec = trans * vec;
	std::cout << vec.x << vec.y << vec.z << std::endl;


	InitGLFW();
	GLFWwindow* window = CreateGLFWWindow();
	if (window == nullptr)
	{
		return 1;
	}
	else
	{
		std::cout << "Window cration Sucess!";
	}
	//Initialise GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	//Need to Adjust the View port agains the windows
	//We will register a callback here.
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	RenderLoop(window);


	//s
	return 0;
}

void InitGLFW()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

}

GLFWwindow* CreateGLFWWindow()
{

	GLFWwindow *window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ZDIS Engine", NULL, NULL);
	if (window == nullptr)
	{
		std::cout << "Failed in Window Creations!";
		return NULL;
	}
	glfwMakeContextCurrent(window);
	return window;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void RenderLoop(GLFWwindow * window)
{

	stbi_set_flip_vertically_on_load(true);
	unsigned int m_iWoodTextureID;
	glGenTextures(1, &m_iWoodTextureID);
	glBindTexture(GL_TEXTURE_2D, m_iWoodTextureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Load the image
	int m_iImgWidth, m_iImgHeight, m_iImgChannels;
	unsigned char * dataWoodContainerTex = stbi_load("D:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Textures/container.jpg", &m_iImgWidth, &m_iImgHeight, &m_iImgChannels, 0);

	if (dataWoodContainerTex)
	{
		/*The first argument specifies the texture target; setting this to GL_TEXTURE_2D means this operation will generate a texture on the currently bound texture object at the same target (so any textures bound to targets GL_TEXTURE_1D or GL_TEXTURE_3D will not be affected).
			The second argument specifies the mipmap level for which we want to create a texture for if you want to set each mipmap level manually, but we'll leave it at the base level which is 0.
			The third argument tells OpenGL in what kind of format we want to store the texture. Our image has only RGB values so we'll store the texture with RGB values as well.
			The 4th and 5th argument sets the width and height of the resulting texture. We stored those earlier when loading the image so we'll use the corresponding variables.
			The next argument should always be 0 (some legacy stuff).
			The 7th and 8th argument specify the format and datatype of the source image. We loaded the image with RGB values and stored them as chars (bytes) so we'll pass in the corresponding values.
		The last argument is the actual image data.*/

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_iImgWidth, m_iImgHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, dataWoodContainerTex);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to Load Texture" << std::endl;

	}
	



	//Cube declaration
	unsigned int  VAOcube;
	CreateCube(VAOcube);
	/*Shader m_refCubeMVPShader("G:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Shaders/ModelCoor/VertexCube.vs",
		"G:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Shaders/ModelCoor/FragmentCubeTwoMixUniformTexture.fs");
*/
	Shader m_refCubeMVPShader = ResourceManager::LoadShader("D:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Shaders/ModelCoor/VertexCube.vs",
		"D:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Shaders/ModelCoor/FragmentCubeTwoMixUniformTexture.fs");


//Load the image

	unsigned int m_iFaceTextureID;
	glGenTextures(1, &m_iFaceTextureID);
	glBindTexture(GL_TEXTURE_2D, m_iFaceTextureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	unsigned char * dataFaceTex = stbi_load("D:/HobbyProject/Opengl/ZDIS_PG/ZDIS_OpenglStudy/ZDIS_Engine/Textures/awesomeface.png", &m_iImgWidth, &m_iImgHeight, &m_iImgChannels, 0);



	if (dataFaceTex)
	{
		/*The first argument specifies the texture target; setting this to GL_TEXTURE_2D means this operation will generate a texture on the currently bound texture object at the same target (so any textures bound to targets GL_TEXTURE_1D or GL_TEXTURE_3D will not be affected).
			The second argument specifies the mipmap level for which we want to create a texture for if you want to set each mipmap level manually, but we'll leave it at the base level which is 0.
			The third argument tells OpenGL in what kind of format we want to store the texture. Our image has only RGB values so we'll store the texture with RGB values as well.
			The 4th and 5th argument sets the width and height of the resulting texture. We stored those earlier when loading the image so we'll use the corresponding variables.
			The next argument should always be 0 (some legacy stuff).
			The 7th and 8th argument specify the format and datatype of the source image. We loaded the image with RGB values and stored them as chars (bytes) so we'll pass in the corresponding values.
		The last argument is the actual image data.*/

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_iImgWidth, m_iImgHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, dataFaceTex);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to Load Texture" << std::endl;
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Model View projection
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));

	//View Matrix
	glm::mat4 view = glm::mat4(1.0f);
	// note that we're translating the scene in the reverse direction of where we want to move
	view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

	//Projection
	glm::mat4 projection;


	unsigned int ModelUniformLocation = glGetUniformLocation(m_refCubeMVPShader.m_iProgramID, "model");
	unsigned int ViewUniformLocation = glGetUniformLocation(m_refCubeMVPShader.m_iProgramID, "view");
	unsigned int ProjectionUniformLocation = glGetUniformLocation(m_refCubeMVPShader.m_iProgramID, "projection");


	glm::vec3 CubePositions[] = {
				  glm::vec3(0.0f,  0.0f,  0.0f),
				  glm::vec3(2.0f,  5.0f, -15.0f),
				  glm::vec3(-1.5f, -2.2f, -2.5f),
				  glm::vec3(-3.8f, -2.0f, -12.3f),
				  glm::vec3(2.4f, -0.4f, -3.5f),
				  glm::vec3(-1.7f,  3.0f, -7.5f),
				  glm::vec3(1.3f, -2.0f, -2.5f),
				  glm::vec3(1.5f,  2.0f, -2.5f),
				  glm::vec3(1.5f,  0.2f, -1.5f),
				  glm::vec3(-1.3f,  1.0f, -1.5f)
	};



	glm::mat4 camLookMat = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	float camX = 0.0f, camY = 0.0f;
	float camRadius = 4.0f;


	CameraPosition = glm::vec3(0, 0, 3.0f);
	CameraDirection = glm::vec3(0, 0, -1.0f);
	CameraUp = glm::vec3(0, 1.0f, 0);




	previousTime = glfwGetTime();


	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	//Camera FPS
	m_SceneCamera = Camera(CameraPosition, CameraUp);

	//Working model
	while (glfwWindowShouldClose(window) == false)
	{
		currentTime = glfwGetTime();

		ProcessInput(window);


		//REndering related stuff

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	
		glEnable(GL_DEPTH_TEST);
		

		CameraDirection = glm::normalize(cameraLookDirection);
		camLookMat = glm::lookAt(CameraPosition, CameraDirection + CameraPosition, CameraUp);

		camLookMat = m_SceneCamera.GetViewMatrix();
		camFOV = m_SceneCamera.m_fZoom;

		projection = glm::perspective(glm::radians(camFOV), ((float)SCR_WIDTH / SCR_HEIGHT), 0.1f, 100.0f);

		for (int i = 0; i < 10; i++)
		{
			model = glm::translate(glm::mat4(1.0f), CubePositions[i]);
			if (i == 0 || i % 3 == 0)
				model = glm::rotate(model, (float)glfwGetTime()*glm::radians(45.0f), glm::vec3(0.5f, 0.7f, 0.0f));
			//DrawCube(m_refCubeMVPShader, VAOcube, model, ModelUniformLocation, view, ViewUniformLocation, projection, ProjectionUniformLocation, m_iWoodTextureID, m_iFaceTextureID);


			DrawCube(m_refCubeMVPShader, VAOcube, model, ModelUniformLocation, camLookMat, ViewUniformLocation, projection, ProjectionUniformLocation, m_iWoodTextureID, m_iFaceTextureID);
		}

		glfwSwapBuffers(window);
		glfwPollEvents();


		deltaTime = currentTime - previousTime;
		previousTime = currentTime;
	}

	



	glfwTerminate();
	std::cout << "glfwTerminateClosing to Window";
	return;
}


bool firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	m_SceneCamera.ProcessMouseInput(xpos, ypos);

	if (firstMouse) // this bool variable is initially set to true
	{
		prevXPos = xpos;
		prevYPos = ypos;
		firstMouse = false;
	}

	deltaX = xpos - prevXPos;
	deltaY = prevYPos - ypos;

	prevXPos = xpos;
	prevYPos = ypos;

	deltaX *= 0.1f;
	deltaY *= 0.1f;

	pitch += deltaY;
	yaw += deltaX;

	if (pitch > 89.0f)
	{
		pitch = 89.0f;
	}
	if (pitch < -89.0f)
	{
		pitch = -89.0f;
	}

	cameraLookDirection.x = cos(glm::radians(pitch))* cos(glm::radians(yaw));
	cameraLookDirection.y = sin(glm::radians(pitch));
	cameraLookDirection.z = cos(glm::radians(pitch))* sin(glm::radians(yaw));



}



void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	m_SceneCamera.ProcessScrollInput(yoffset);

	if (camFOV >= 1.0f && camFOV <= 60.0f)
	{
		camFOV += yoffset;
	}

	if (camFOV < 1.0f)
	{
		camFOV = 1.0f;
	}

	if (camFOV > 60.0f)
	{
		camFOV = 60.0f;
	}
}

void ProcessInput(GLFWwindow *a_window)
{

	float KeyMoveSpeed = 0.5f;
	if (glfwGetKey(a_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(a_window, true);
		std::cout << "Closing to Window";
	}
	if (glfwGetKey(a_window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		m_fMixValue += 0.1f*deltaTime;

	}
	if (glfwGetKey(a_window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		m_fMixValue -= 0.1f*deltaTime;
	}

	//Camera simple movements
	if (glfwGetKey(a_window, GLFW_KEY_W) == GLFW_PRESS)
	{
		CameraPosition += deltaTime * CameraDirection*CameraMoveSpeed;
		m_SceneCamera.ProcessKeyboardInput(eCameraMovement::FORWARD, deltaTime);

	}
	if (glfwGetKey(a_window, GLFW_KEY_S) == GLFW_PRESS)
	{
		CameraPosition -= deltaTime * CameraDirection*CameraMoveSpeed;
		m_SceneCamera.ProcessKeyboardInput(eCameraMovement::BACKWARD, deltaTime);
	}
	if (glfwGetKey(a_window, GLFW_KEY_A) == GLFW_PRESS)
	{
		CameraPosition += deltaTime * (glm::cross(CameraUp, CameraDirection))*CameraMoveSpeed;
		m_SceneCamera.ProcessKeyboardInput(eCameraMovement::RIGHT, deltaTime);
	}

	if (glfwGetKey(a_window, GLFW_KEY_D) == GLFW_PRESS)
	{
		CameraPosition -= deltaTime * (glm::cross(CameraUp, CameraDirection))*CameraMoveSpeed;
		m_SceneCamera.ProcessKeyboardInput(eCameraMovement::LEFT, deltaTime);
	}
}







void CreateCube(unsigned int &a_VAO)
{
	unsigned int VBO, EBO;
	glGenVertexArrays(1, &a_VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(a_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VerticesForCube), VerticesForCube, GL_STATIC_DRAW);

	//	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)(sizeof(GL_FLOAT) * 3));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

void DrawCube(Shader a_ShaderToUse, unsigned int &a_VAO, glm::mat4 model, unsigned int ModelUniformLocation,
	glm::mat4 view, unsigned int ViewUniformLocation,
	glm::mat4 projection, unsigned int ProjectionUniformLocation,
	unsigned int m_iWoodTextureID,
	unsigned int m_iFaceTextureID)
{
	a_ShaderToUse.Use();
	a_ShaderToUse.SetFloat("iMixValue", m_fMixValue);
	a_ShaderToUse.SetInt("ourTexture1", 0);
	a_ShaderToUse.SetInt("ourTexture2", 1);

	glUniformMatrix4fv(ModelUniformLocation, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(ViewUniformLocation, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(ProjectionUniformLocation, 1, GL_FALSE, glm::value_ptr(projection));

	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_iWoodTextureID);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_iFaceTextureID);

	glBindVertexArray(a_VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);

}