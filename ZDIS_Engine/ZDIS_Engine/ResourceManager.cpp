#include "ResourceManager.h"





ResourceManager::ResourceManager()
{
}


Shader ResourceManager::LoadShader(const char* a_VertexPath, const char* a_FragmentPath)
{
	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;

	//Letting shader file to throw expections...
	vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		//Open file!
		vShaderFile.open(a_VertexPath);
		fShaderFile.open(a_FragmentPath);
		std::stringstream vShaderStream, fShaderStream;
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();
		vShaderFile.close();
		fShaderFile.close();

		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "[ResourceManager](LoadShader)Error:Coudn't Read the files" << std::endl;
		exit(1);
	}
	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();

	Shader createdShader(vShaderCode, fShaderCode, 1);
	//Do all the map things..

	return createdShader;
}

Texture ResourceManager::LoaderTexture(const char* a_TexturePath)
{

	Texture createdTexture = LoadTextureFromFile(a_TexturePath);
	//Do all the map things..

	return createdTexture;
}

Texture ResourceManager::LoadTextureFromFile(const char* a_TexturePath)
{
	Texture createdTexture;
	unsigned char * textureContent = stbi_load(a_TexturePath, &createdTexture.m_iImageWidth, &createdTexture.m_iImageHeight,
		&createdTexture.m_iImgChannel, 0);

	if (textureContent)
	{
		//Create texture setup properly
		createdTexture.GenerateTexture(createdTexture.m_iImageWidth, createdTexture.m_iImageHeight, textureContent);
	}
	else
	{
		std::cout << "[ResourceManager]:Failed to Load Texture"<< a_TexturePath << std::endl;

	}


	return createdTexture;
}


ResourceManager::~ResourceManager()
{
}
