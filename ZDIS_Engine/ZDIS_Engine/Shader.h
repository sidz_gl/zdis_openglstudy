#pragma once
#include <glad/glad.h> // include glad to get all the required OpenGL headers

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>


class Shader
{
public:
	unsigned int m_iProgramID;
	Shader(const char* a_VertexPath, const char* a_FragmentPath);
	
	Shader(const char* a_vShaderCode, const char* f_vShaderCode,int a);
	
	~Shader();

	void Use();

	void SetBool(const std::string &a_UniformName, bool a_bValue);
	void SetFloat(const std::string &a_UniformName, float a_bValue);
	void SetInt(const std::string &a_UniformName, int a_bValue);
//private :
	//void CheckCompileErrors();//TODO change to a different function

};

