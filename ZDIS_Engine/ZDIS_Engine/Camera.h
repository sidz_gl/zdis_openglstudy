#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


enum eCameraMovement
{
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

const float c_fYAW = -90.0f;
const float  c_fPITCH = 0.0f;
const float  c_fSpeed = 2.5f;
const float  c_fSensitivity = 0.1f;
const float  c_fZoom = 45.0f;

class Camera
{
public:


	glm::vec3 m_Position;
	glm::vec3 m_Front;
	glm::vec3 m_Up;
	glm::vec3 m_Right;

	float m_fPitch;
	float m_fYaw;
	float m_fZoom;

	float m_fMouseSpeed;
	float m_fSensitivity;
	
	Camera();
	Camera(glm::vec3 a_Position, glm::vec3 a_Up, float a_fPitch = c_fPITCH, float a_fYaw = c_fYAW);// :m_Front(glm::vec3(0, 0, -1.0f)), m_fSensitivity(c_fSensitivity), m_fMouseSpeed(c_fSpeed), m_fZoom(c_fZoom);
	~Camera();

	glm::mat4 GetViewMatrix();
	void ProcessKeyboardInput(eCameraMovement a_eMovementType,float a_fdeltaTime);
	void ProcessMouseInput(float a_fXpos,float a_fYPos);
	void ProcessScrollInput(float a_fYPos);

private:
	void UpdateCameraValues();
	float m_fPreviousX;
	float m_fPreviousY;
	float m_fDeltaX;
	float m_fDeltaY;
	bool m_bFirstMouse;
};

