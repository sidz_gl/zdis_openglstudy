#version 330 core
out vec4 FragColor;
  
in vec4 vertexColor; // the input variable from the vertex shader (same name and same type)  
in vec2 TexCord; // the input variable from the vertex shader (same name and same type)  

uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;
void main()
{
    //FragColor = vertexColor;
	

	 FragColor = mix(texture(ourTexture1, TexCord), texture(ourTexture2, vec2(1.0-TexCord.x,TexCord.y) ), 0.2);
}