#version 330 core
layout (location = 0) in vec3 aPos; // the position variable has attribute position 0
layout (location = 1) in vec3 aCol; // the color variable has attribute position 1
layout (location = 2) in vec2 ainTexCoord; // the TextureSV variable has attribute position 2
  
out vec4 vertexColor; // specify a color output to the fragment shader
out vec2 TexCord; // specify a color output to the fragment shader


void main()
{
    gl_Position = vec4(aPos, 1.0); // see how we directly give a vec3 to vec4's constructor
    vertexColor = vec4(0.5, 0.0, 0.0, 1.0); // set the output variable to a dark-red color
	TexCord = ainTexCoord;//Passing in Frag  
}